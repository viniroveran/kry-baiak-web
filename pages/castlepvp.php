﻿<?php
if(!defined('INITIALIZED'))
    exit;
$main_content .= '
<center>
			<table>
				<tbody>
					<tr>
						<td><img src="./layouts/tibiarl/images/global/content/headline-bracer-left.gif"></td>
						<td style="text-align:center;vertical-align:middle;horizontal-align:center;font-size:17px;font-weight:bold;">Castle PvP<br></td>
						<td><img src="./layouts/tibiarl/images/global/content/headline-bracer-right.gif"></td>
					</tr>
				</tbody>
			</table>
		</center>
		<br><br>
<table width="100%" border="0" cellpadding="4" cellspacing="1">
    <tbody>
        <tr>
            <td class="white" colspan="3" bgcolor="#505050"><span class="style4">Informações</span></td>
        </tr>
        <tr>
            <td colspan="3" bgcolor="#d4c0a1"><span class="style4">
            Um evento de dominar castelo que fica ativo 24 horas por dia para todas as guilds no servidor, com objetivo de aumentar a diversão dos que gostam de PvP.
            </span></td>
        </tr>
        <tr>
            <td class="white" colspan="3" bgcolor="#505050">
                <span class="style4">
                    <center><b>Como posso fazer isso?</b></center>
                </span>
            </td>
        </tr>
        <tr>
            <td colspan="3" bgcolor="#f1e0c6">
            <span class="style4">O teleport para acessar Castle PvP está disponível sempre no templo.
             Todas as vezes que o limite do castelo for ultrapassado, uma mensagem será enviada para todo o servidor para que todos saibam que o castelo está sendo invadido.
             Quando se acessa a area de dominação, aparecem quatro Geradores de Crystal que devem ser destruídos. Assim que todos forem destruidos, a primeira Guild a puxar a alavanca no final será a dominante do Castle 24 HRs.<br>
             <br><b>OBS¹</b>: Geradores Crystal tem um Respawn de 5 Minutos.
             <br><b>OBS²</b>: Membros da guild vencedora atual tem um teleport de atalho dentro do castelo para poder ajudar na proteçao.
             <br><b>OBS³</b>: Dentro das areas do Castle, como "Hunts", se você morrer, não perderá level. Mas, fora do castle, nas areas de dominação, você perderá level.
             </span></td>
        </tr>
        <tr>
            <td colspan="3" bgcolor="#d4c0a1"><span class="style4"><center><img src="./layouts/tibiarl/images/events/castle24/castle24.png"></center></span></td>
        </tr>
        <tr>
            <td class="white" colspan="3" bgcolor="#505050">
                <span class="style4"><center>A guild dominante tem previlégios</center></span>
            </td>
        </tr>
        <tr>
            <td colspan="3" bgcolor="#f1e0c6">
                <span class="style4"><i><center>Área do castelo exclusiva para os players da guild dominante.</center></i></span>
            </td>
        </tr>
    </tbody>
</table>';