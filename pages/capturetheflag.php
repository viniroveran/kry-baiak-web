<?php
$main_content .= '
        <center>
			<table>
				<tbody>
					<tr>
						<td><img src="./layouts/tibiarl/images/global/content/headline-bracer-left.gif"></td>
						<td style="text-align:center;vertical-align:middle;horizontal-align:center;font-size:17px;font-weight:bold;">Capture the Flag<br></td>
						<td><img src="./layouts/tibiarl/images/global/content/headline-bracer-right.gif"></td>
					</tr>
				</tbody>
			</table>
		</center>
		<br><br>
<table width="100%" border="0" cellpadding="4" cellspacing="1">
    <tbody>
    <tr>
        <td class="white" colspan="3" bgcolor="#505050"><span class="style4">Informações</span></td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#f1e0c6"><span class="style4">É um evento de aonde cada time tem o objetivo de roubar a bandeira do outro time. </span></td>
    </tr>

    <tr>
        <td colspan="3" bgcolor="#f1e0c6">
                <span class="style4">
                    Todos os dias no horario de 18:00 será aberto no templo um teleport para o evento, oonde quem entrar não vai poder sair até que ele acabe, você ficará em uma sala de espera durante 5 minutos (tempo para mais participantes entrarem). Depois que se passarem 5 minutos o evento terá inicio,será formado 2 equipes aleatorias, o objetivo é roubar a bandeira do outro time e trazer para sua base, o primeiro time que fizer isso 10 vezes, será o time ganhador do evento!!.
                    <br>
                    <br><b>OBS:</b> Morrer no evento não perde level.
                    <br><b>OBS:</b> Se você demorar muito para levar a bandeira para sua base ela volta para o lugar de origem.
                    <br><b>OBS:</b>
                    Não é permitido usar nenhum tipo de magia para ficar invisivel ou stealth ring.
                </span>
        </td>
    </tr>
    <td colspan="3" bgcolor="#d4c0a1">
        <span class="style4">
            <center><img src="./layouts/tibiarl/images/events/capturetheflag/capturetheflag.png"></center>
        </span>
    </td>

    <tr>
        <td class="white" colspan="3" bgcolor="#505050">
                <span class="style4">
				    <center>Qual horario do evento?</center>
			    </span>
        </td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#f1e0c6">
                <span class="style4">
				    <i>Todos os dias as 18:00.
			    </span>
        </td>
    </tr>

    <tr>
        <td class="white" colspan="3" bgcolor="#505050">
                <span class="style4">
				    <center>Quais são os benefícios para o time que vencer??</center>
			    </span>
        </td>
    </tr>

    <tr>
    <tr bgcolor="#d4c0a1"></tr>
    <tr>
        <tr>
            <tr>
                <td colspan="3" bgcolor="#f1e0c6">
                    <span class="style4">
                        <i><img src="./layouts/tibiarl/images/events/capturetheflag/19event.png"><br>Cada jogador do time vencedor 80 Event Coins e + <font color="green"><b>35000000</b></font> de expêriencia.
                    </span>
                </td>
            </tr>
        <tr>
            <td class="white" colspan="3" bgcolor="#505050">
                <span class="style4">
                    <center>E se o jogo empatar??</center>
                </span>
            </td>
        </tr>
    <tr>
    <tr bgcolor="#d4c0a1"></tr>
    <tr>
        <td colspan="3" bgcolor="#f1e0c6">
            <span class="style4">
                <i><img src="./layouts/tibiarl/images/events/capturetheflag/18event.png">
                    <br>
                    Todos os jogadores ganharam 50 Event Coin + <font color="green"><b>20000000</b></font> de expêriencia.
            </span>
        </td>
    </tr>
    <tr>
    </tbody>
</table>
</center>
<div style="text-align: right;">
    <font color="0000FF"><b></b></font>
</div>
<br>

<div class="TableContainer">
    <div class="CaptionContainer">
        <div class="CaptionInnerContainer">
            <span class="CaptionEdgeLeftTop" style="background-image:url(./layouts/tibiarl/images/content/box-frame-edge.gif);"></span>
            <span class="CaptionEdgeRightTop" style="background-image:url(./layouts/tibiarl/images/content/box-frame-edge.gif);"></span>
            <span class="CaptionBorderTop" style="background-image:url(./layouts/tibiarl/images/content/table-headline-border.gif);"></span>
            <span class="CaptionVerticalLeft" style="background-image:url(./layouts/tibiarl/images/content/box-frame-vertical.gif);"></span>
            <div class="Text">NPC Event Seller</div>
            <span class="CaptionVerticalRight" style="background-image:url(./layouts/tibiarl/images/content/box-frame-vertical.gif);"></span>
            <span class="CaptionBorderBottom" style="background-image:url(./layouts/tibiarl/images/content/table-headline-border.gif);"></span>
            <span class="CaptionEdgeLeftBottom" style="background-image:url(./layouts/tibiarl/images/content/box-frame-edge.gif);"></span>
            <span class="CaptionEdgeRightBottom" style="background-image:url(./layouts/tibiarl/images/content/box-frame-edge.gif);"></span>
        </div>
    </div>
    <table class="Table5" cellpadding="0" cellspacing="0">
        <tbody>
        <tr>
            <td>
                <div class="InnerTableContainer">
                    <table style="width:100%;">
                        <tbody>
                        <tr>
                            <td>
                                <div class="TableShadowContainerRightTop" >
                                    <div class="TableShadowRightTop" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rt.gif);" ></div>
                                </div>
                                <div class="TableContentAndRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rm.gif);" >
                                    <div class="TableContentContainer" >
                                        <table class="TableContent" width="100%" style="border:1px solid #faf0d7;" >
                                            <tr>
                                                <td>O NPC <b>Event Seller</b> fica localizado no templo da cidade principal, esse NPC vende itens exclusivos do server e s&oacute; negocia com a moeda Event Coins. Veja mais abaixo as op&ccedil;&otilde;es de itens a venda e os seus devidos valores.</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="TableShadowContainer" >
                                    <div class="TableBottomShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bm.gif);" >
                                        <div class="TableBottomLeftShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bl.gif);" ></div>
                                        <div class="TableBottomRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-br.gif);" ></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="TableShadowContainerRightTop" >
                                    <div class="TableShadowRightTop" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rt.gif);" ></div>
                                </div>
                                <div class="TableContentAndRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rm.gif);" >
                                    <div class="TableContentContainer" >
                                        <table class="TableContent" width="100%"  style="border:1px solid #faf0d7;" >
                                            <tr>
                                                <td><b>Double Exp</b> Fica com experiencia dobrada por 2 horas.</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="TableShadowContainer" >
                                    <div class="TableBottomShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bm.gif);" >
                                        <div class="TableBottomLeftShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bl.gif);" ></div>
                                        <div class="TableBottomRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-br.gif);" ></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="TableShadowContainerRightTop" >
                                    <div class="TableShadowRightTop" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rt.gif);" ></div>
                                </div>
                                <div class="TableContentAndRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rm.gif);" >
                                    <div class="TableContentContainer" >
                                        <table class="TableContent" width="100%"  style="border:1px solid #faf0d7;" >
                                            <tr>
                                                <td width="50%"><b>Valor: 140 Event Coin Coins</b></br><img src="./layouts/tibiarl/images/events/capturetheflag/7event.png"></td><td width="50%"><b>Double Exp</b></br><img src="./layouts/tibiarl/images/events/capturetheflag/6event.png"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="TableShadowContainer" >
                                    <div class="TableBottomShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bm.gif);" >
                                        <div class="TableBottomLeftShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bl.gif);" ></div>
                                        <div class="TableBottomRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-br.gif);" ></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="TableShadowContainerRightTop" >
                                    <div class="TableShadowRightTop" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rt.gif);" ></div>
                                </div>
                                <div class="TableContentAndRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rm.gif);" >
                                    <div class="TableContentContainer" >
                                        <table class="TableContent" width="100%"  style="border:1px solid #faf0d7;" >
                                            <tr>
                                                <td><b>1 Cave Exclusiva</b><b></b> com hunts exclusivas por 6 horas.</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="TableShadowContainer" >
                                    <div class="TableBottomShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bm.gif);" >
                                        <div class="TableBottomLeftShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bl.gif);" ></div>
                                        <div class="TableBottomRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-br.gif);" ></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="TableShadowContainerRightTop" >
                                    <div class="TableShadowRightTop" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rt.gif);" ></div>
                                </div>
                                <div class="TableContentAndRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rm.gif);" >
                                    <div class="TableContentContainer" >
                                        <table class="TableContent" width="100%"  style="border:1px solid #faf0d7;" >
                                            <tr>
                                                <td width="50%"><b>Valor: 250 Event Coins</b><br><img src="./layouts/tibiarl/images/events/capturetheflag/8event.png"></td><td width="50%"><b>Cave Exclusiva</b><br><img src="./layouts/tibiarl/images/events/capturetheflag/9event.png"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="TableShadowContainer" >
                                    <div class="TableBottomShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bm.gif);" >
                                        <div class="TableBottomLeftShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bl.gif);" ></div>
                                        <div class="TableBottomRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-br.gif);" ></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="TableShadowContainerRightTop" >
                                    <div class="TableShadowRightTop" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rt.gif);" ></div>
                                </div>
                                <div class="TableContentAndRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rm.gif);" >
                                    <div class="TableContentContainer" >
                                        <table class="TableContent" width="100%"  style="border:1px solid #faf0d7;" >
                                            <tr>
                                                <td><b>Double Exp </b> adiciona <b>15 minutos</b> de expêriencia bonus.</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="TableShadowContainer" >
                                    <div class="TableBottomShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bm.gif);" >
                                        <div class="TableBottomLeftShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bl.gif);" ></div>
                                        <div class="TableBottomRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-br.gif);" ></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="TableShadowContainerRightTop" >
                                    <div class="TableShadowRightTop" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rt.gif);" ></div>
                                </div>
                                <div class="TableContentAndRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rm.gif);" >
                                    <div class="TableContentContainer" >
                                        <table class="TableContent" width="100%"  style="border:1px solid #faf0d7;" >
                                            <tr>
                                                <td width="50%"><b>Valor: 50 Event Coins</b><br><img src="./layouts/tibiarl/images/events/capturetheflag/10event.png"></td><td width="50%"><b>Double Exp 15 min</b><br><img src="./layouts/tibiarl/images/events/capturetheflag/11event.png"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="TableShadowContainer" >
                                    <div class="TableBottomShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bm.gif);" >
                                        <div class="TableBottomLeftShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bl.gif);" ></div>
                                        <div class="TableBottomRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-br.gif);" ></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="TableShadowContainerRightTop" >
                                    <div class="TableShadowRightTop" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rt.gif);" ></div>
                                </div>
                                <div class="TableContentAndRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rm.gif);" >
                                    <div class="TableContentContainer" >
                                        <table class="TableContent" width="100%"  style="border:1px solid #faf0d7;" >
                                            <tr>
                                                <td><b>Critical Stone</b> Ganha <b>1</b> Critical Stone para evoluir seu level de skill.</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="TableShadowContainer" >
                                    <div class="TableBottomShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bm.gif);" >
                                        <div class="TableBottomLeftShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bl.gif);" ></div>
                                        <div class="TableBottomRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-br.gif);" ></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="TableShadowContainerRightTop" >
                                    <div class="TableShadowRightTop" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rt.gif);" ></div>
                                </div>
                                <div class="TableContentAndRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rm.gif);" >
                                    <div class="TableContentContainer" >
                                        <table class="TableContent" width="100%"  style="border:1px solid #faf0d7;" >
                                            <tr>
                                                <td width="50%"><b>Valor: 80 Event Coins</b><br><img src="./layouts/tibiarl/images/events/capturetheflag/15event.png"></td><td width="50%"><b>Critical Stone</br><img src="./layouts/tibiarl/images/events/capturetheflag/13event.png"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="TableShadowContainer" >
                                    <div class="TableBottomShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bm.gif);" >
                                        <div class="TableBottomLeftShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bl.gif);" ></div>
                                        <div class="TableBottomRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-br.gif);" ></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="TableShadowContainerRightTop" >
                                    <div class="TableShadowRightTop" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rt.gif);" ></div>
                                </div>
                                <div class="TableContentAndRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rm.gif);" >
                                    <div class="TableContentContainer" >
                                        <table class="TableContent" width="100%"  style="border:1px solid #faf0d7;" >
                                            <tr>
                                                <td><b>Dodge Stone</b> Ganha <b>1</b> Dodge Stone para evoluir seu level de skill.</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="TableShadowContainer" >
                                    <div class="TableBottomShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bm.gif);" >
                                        <div class="TableBottomLeftShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bl.gif);" ></div>
                                        <div class="TableBottomRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-br.gif);" ></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="TableShadowContainerRightTop" >
                                    <div class="TableShadowRightTop" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rt.gif);" ></div>
                                </div>
                                <div class="TableContentAndRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rm.gif);" >
                                    <div class="TableContentContainer" >
                                        <table class="TableContent" width="100%"  style="border:1px solid #faf0d7;" >
                                            <tr>
                                                <td width="50%"><b>Valor: 60 Event Coin</b><br><img src="./layouts/tibiarl/images/events/capturetheflag/12event.png"></td><td width="50%"><b>Dodge Stone</b></br><img src="./layouts/tibiarl/images/events/capturetheflag/14event.png"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="TableShadowContainer" >
                                    <div class="TableBottomShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bm.gif);" >
                                        <div class="TableBottomLeftShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bl.gif);" ></div>
                                        <div class="TableBottomRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-br.gif);" ></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="TableShadowContainerRightTop" >
                                    <div class="TableShadowRightTop" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rt.gif);" ></div>
                                </div>
                                <div class="TableContentAndRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rm.gif);" >
                                    <div class="TableContentContainer" >
                                        <table class="TableContent" width="100%"  style="border:1px solid #faf0d7;" >
                                            <tr>
                                                <td><b>Stamina</b> Ganha <b>1</b> Stamina Potion que deixa sua stamina full.</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="TableShadowContainer" >
                                    <div class="TableBottomShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bm.gif);" >
                                        <div class="TableBottomLeftShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bl.gif);" ></div>
                                        <div class="TableBottomRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-br.gif);" ></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="TableShadowContainerRightTop" >
                                    <div class="TableShadowRightTop" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rt.gif);" ></div>
                                </div>
                                <div class="TableContentAndRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-rm.gif);" >
                                    <div class="TableContentContainer" >
                                        <table class="TableContent" width="100%"  style="border:1px solid #faf0d7;" >
                                            <tr>
                                                <td width="50%"><b>Valor: 50 Event Coins</b><br><img src="./layouts/tibiarl/images/events/capturetheflag/17event.png"></td><td width="50%"><b>Stamina Potion</b></br><img src="./layouts/tibiarl/images/events/capturetheflag/16event.png"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="TableShadowContainer" >
                                    <div class="TableBottomShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bm.gif);" >
                                        <div class="TableBottomLeftShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-bl.gif);" ></div>
                                        <div class="TableBottomRightShadow" style="background-image:url(./layouts/tibiarl/images/global/content/table-shadow-br.gif);" ></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </td>
    </tr>
</tbody>
</table>
</div>';