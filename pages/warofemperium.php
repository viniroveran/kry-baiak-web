<?php
if(!defined('INITIALIZED'))
    exit;
$woe = $SQL->query("
	SELECT w.id AS id, w.time AS time, g.name AS guild, p.name AS name, w.started AS start, w.guild AS guild_id
		FROM woe AS w
	INNER JOIN players AS p
		ON p.id = w.breaker
	INNER JOIN guilds AS g
		ON g.id = w.guild
	ORDER BY id DESC LIMIT 10;	
");

foreach ($woe as $k=>$v) {
	$winners .="
		<TR BGCOLOR=\"".$config['site'][($k % 2 == 1 ? 'light' : 'dark').'border']."\">
			<TD>{$v[id]}</TD>
			<TD><a href='?subtopic=guilds&action=show&guild=" . $v[guild_id] . "'>$v[guild]</a></TD>
			<TD>{$v[name]}</TD>
			<TD>" . date("d/m/y   H:i:s", $v[start]) . "</TD>
			<TD>" . date("d/m/y   H:i:s", $v[time]) . "</TD>
		</TR>
	";
}
$main_content .= '
        <center>
			<table>
				<tbody>
					<tr>
						<td><img src="./layouts/tibiarl/images/global/content/headline-bracer-left.gif"></td>
						<td style="text-align:center;vertical-align:middle;horizontal-align:center;font-size:17px;font-weight:bold;">War of Emperium<br></td>
						<td><img src="./layouts/tibiarl/images/global/content/headline-bracer-right.gif"></td>
					</tr>
				</tbody>
			</table>
		</center>
		<br><br>
';

if(!$winners) {
	$main_content .= '
		<TABLE BORDER=0 CELLSPACING=1 CELLPADDING=4 WIDTH=100%>
			<TR BGCOLOR="'.$config['site']['vdarkborder'].'">
				<TD CLASS=white>
					<B>Vencedor do War of Emperium</B>
				</TD>
			</TR>
			<TR BGCOLOR='.$config['site']['darkborder'].'>
				<TD>
					Ainda n&atilde;o h&aacute; vencedores!
				</TD>
			</TR>
		</TABLE>
		<br>';
} else {
	$main_content .= "
		<TABLE BORDER=0 CELLSPACING=1 CELLPADDING=4 WIDTH=100%>
			<TR BGCOLOR=\"{$config['site']['vdarkborder']}\">
				<TD CLASS=white width=5%>
					<B>No.</B>
				</TD>
				<TD CLASS=white width=30%>
					<B>Winner guild</B>
				</TD>
				<TD CLASS=white width=25%>
					<B>Conquest by</B>
				</TD>
				<TD CLASS=white width=20%>
					<B>Start time</B>
				</TD>
				<TD CLASS=white width=20%>
					<B>Last conquest</B>
				</TD>
			</TR>
			$winners
		</TABLE>
	";
}
$main_content .='
		<table width="100%" cellspacing="1" cellpadding="6">
			<tr widht="100%" bgcolor="#505050">
				<th align="center">Informa&ccedil;&otilde;es</th>
			</tr>
		
			<tr widht="100%" bgcolor="#D4C0A1">
				<td>Todos os dias às <font color="red"><b>21:00</b></font> horas, as Guilds tentam dominar o castelo combatendo seus inimigos até as 22:30.</td>
			</tr>
			<tr widht="100%" bgcolor="#F1E0C6">
				<td>A guild que tiver o dominio do castelo ganhará acesso a <b>hunts exclusivas</b> durante o periodo que estiver dominando.</td>
			</tr>
			
			<tr widht="100%" bgcolor="#F1E0C6"></tr>
		
			<tr widht="100%" bgcolor="#D4C0A1">
				<td><b>Durante 1 hora e 30 minutos</b>, as Guilds tem que tentar tomar o m&aacute;ximo de controle do PvP Castle, ela ter&aacute; que defender o seu dom&iacute;nio no castelo evitando que outras guilds dominem o Castle, isso durante o tempo restante do castelo. </td>
			</tr>
		
			<tr widht="100%" bgcolor="#F1E0C6">
				<td>Sempre que o evento come&ccedil;ar, aparecerá um teleporte no <b>Event Room</b> poder&aacute; ser acessado por todos que possuirem guild.</td>
			</tr>
		</table>
	</center>
';
