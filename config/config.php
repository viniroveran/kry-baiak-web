<?php
# Server infos
$config['site']['server_ip'] = "baiak.krynetwork.com.br";
$config['site']['server_port'] = "7171";
$config['site']['server_version'] = "8.60";
$config['site']['server_desc'] = '<font size="2px">Bem vindo ao <b><font color="green">Kry Baiak</font></b>, veja aqui <a href="/?subtopic=serverinfo">nossas rates</a>.<br><br>
 Nosso objetivo é ser um servidor baiak não "zoado", ou seja, com mapa organizado, PvP balanceado, boas áreas de hunt e eventos divertidos.<br>
 <br>Estamos sempre arrumando bugs, implementando ideias e conteúdo novo! <b>Tem uma sugestão? Nos diga no <a href="/?subtopic=forum">forum</a>!</b> 
 Temos <font color="green">vários eventos</font> e minigames para você se divertir com seus amigos como por exemplo, Bomberman, SnowBall War, Zombie Event! 
 Veja todos os eventos disponíveis na aba "Events" do site!</font>';

# Outfitter configs
$config['site']['outfit_images_url'] = "/outfits/outfit.php";

# Shop configs
$outfits_list = array();
$loyalty_title = array(50 => 'Scout', 100 => 'Sentinel', 200 => 'Steward', 400 => 'Warden', 1000 => 'Squire', 2000 => 'Warrior', 3000 => 'Keeper', 4000 => 'Guardian', 5000 => 'Sage');
$config['shop']['newitemdays'] = 1; #days to item be new in shop

# Pagseguro configs
$config['pagseguro']['testing'] = false;
$config['pagseguro']['tokentest'] = "";
$config['pagseguro']['email'] = '';
$config['pagseguro']['apitoken'] = '';

# Bank transfer data
$config['banktransfer']['bank'] = "Caixa Econômica";
$config['banktransfer']['agency'] = "";
$config['banktransfer']['account'] = "";
$config['banktransfer']['name'] = "";
$config['banktransfer']['operation'] = 003; // 0 = no operation

# PayPal configs
$config['paypal']['email'] = "";

# Social Networks
$config['social']['status'] = true;
$config['social']['facebook'] = "https://www.facebook.com/tibia";

# PAGE: characters.php
$config['site']['quests'] = array(
"Demon Helmet" => 2213,
"In Service of Yalahar" => 12279,
"Pits Of Inferno" => 10544,
"The Ancient Tombs" => 50220,
"The Annihilator" => 2215,
"The Demon Oak" => 1010,
"Wrath Of The Emperor" => 12374);

# time in days to show the former names
$config['site']['formerNames'] = 10;
$config['site']['formerNames_amount'] = 10;

# Account Maker Config
$config['site']['serverPath'] = "/home/krynetwo/tibiafiles/"; 
$config['site']['useServerConfigCache'] = false;
$towns_list = array(
2 => 'Baiak',
);
# this is important if you want to use ajax check in your create account
$config['site']['sqlHost'] = "us.krynetwork.com.br";
$config['site']['sqlUser'] = "remkrycon";
$config['site']['sqlPass'] = "gt0aVizp!h%k";
$config['site']['sqlBD'] = "baiak";

# Create Account Options
$config['site']['one_email'] = true;
$config['site']['create_account_verify_mail'] = true;
$config['site']['verify_code'] = true;
$config['site']['email_days_to_change'] = 7;
$config['site']['newaccount_premdays'] = 0;
$config['site']['send_register_email'] = true;

# Create Character Options
$config['site']['newchar_vocations'] = array(1 => 'Sorcerer Sample', 2 => 'Druid Sample', 3 => 'Paladin Sample', 4 => 'Knight Sample');//if you wan't use rook, put array(0 => 'Rook Sample'); 
$config['site']['newchar_towns'] = array(2);
$config['site']['max_players_per_account'] = 10;


# Emails Config
$config['site']['send_emails'] = true;
$config['site']['mail_address'] = "naoresponda@krynetwork.com.br";
$config['site']['mail_senderName'] = "Suporte Kry Network";
$config['site']['smtp_enabled'] = true;
$config['site']['smtp_host'] = "mail.krynetwork.com.br";
$config['site']['smtp_port'] = 465;
$config['site']['smtp_auth'] = true;
$config['site']['smtp_user'] = "naoresponda@krynetwork.com.br";
$config['site']['smtp_pass'] = "!2#vini!2#";
$config['site']['smtp_secure'] = true;

# PAGE: accountmanagement.php
$config['site']['send_mail_when_change_password'] = true;
$config['site']['send_mail_when_generate_reckey'] = true;
$config['site']['email_time_change'] = 7;
$config['site']['daystodelete'] = 7;
$config['site']['flash_client_enabled'] = false;

# PAGE: guilds.php
$config['site']['guild_need_level'] = 8;
$config['site']['guild_need_pacc'] = false;
$config['site']['guild_image_size_kb'] = 50;
$config['site']['guild_description_chars_limit'] = 2000;
$config['site']['guild_description_lines_limit'] = 6;
$config['site']['guild_motd_chars_limit'] = 250;

# PAGE: adminpanel.php
$config['site']['access_admin_panel'] = 3;

# PAGE: latestnews.php
$config['site']['news_limit'] = 6;

# PAGE: killstatistics.php
$config['site']['last_deaths_limit'] = 40;

# PAGE: team.php
$config['site']['groups_support'] = array(2, 3, 4, 5, 6);

# PAGE: highscores.php
$config['site']['groups_hidden'] = array(3, 4, 5, 6);
$config['site']['accounts_hidden'] = array(1);

# PAGE: lostaccount.php
$config['site']['email_lai_sec_interval'] = 180;

#  Pagseguro offers
// ValorR$ => Qtd_coins
$config['pagseguro']['offers'] = array(
    500=>75,
    800=>125,
    1500=>250,
    2800=>500,
    4900=>1000
);
// Nome do produto
$config['pagseguro']['produtoNome'] = 'Tibia Coins';

$config['pagseguro']['urlRedirect'] =  'https://127.0.0.1/';
$config['pagseguro']['urlNotification'] = 'https://127.0.0.1/retpagseguro.php';

# Layout Config
$config['site']['layout'] = 'tibiarl';
$config['site']['vdarkborder'] = '#505050';
$config['site']['darkborder'] = '#D4C0A1';
$config['site']['lightborder'] = '#F1E0C6';
$config['site']['download_page'] = false;
$config['site']['serverinfo_page'] = true;