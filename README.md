#Kry Tibia Web
For Ubuntu 16.04 using Nginx + Php7.0 FPM

##Required PHP Packages 

```
sudo apt-get install -y php7.0-cgi php7.0-cli php7.0-common php7.0-curl php7.0-fpm php7.0-gd php7.0-json php7.0-mbstring php7.0-mcrypt php7.0-mysql php7.0-opcache php7.0-readline php7.0-xml php7.0-zip php-apcu
```

##Database
```
sudo apt-get install mysql-common mysql-server mysql-client
```

##Recommended editing tools
```
PhpStorm

sudo apt-get install mysql-workbench
```
